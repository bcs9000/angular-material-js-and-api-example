# Angular Material JS + Api communication

I had done this little project for a study. 
There are 2 Json files which I populated with some random information. Not Loren Ipsum, but some Jojo Bizarre Adventure info I got from Crunchy Roll (in portuguese).
I hope it helps ;)

### Features:

  - Content reading via API 
  - Angular for dealing with API input, interactive components (tabs)
  - Angular JS Material for interactive components, page boilerplate and responsive treatments
  - Pug for dealing with complex HTML
  - SASS for dealing with complex and custom CSS (specially accordion "component")
  - Gulp for a better workflow form HTML, CSS, JS, even image files. Gulp serve was important for real-time preview of the application
  - No JQuery

### Installation

This project requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd <PROJECT DIRECTORY>
$ npm install -d
$ gulp
```

I used [JSON Bin](https://jsonbin.io/) for hosting the JSON files for this project (epis.json and info.json), they are called on src/js/app.controller.js
### General task commands

Here goes the most used commands for testing and deploying assets of the application. These commands are based on a group of internal task commands for debugging, compiling and serving the application and its assets as well.

Serving the application via http
```sh
$ gulp serve
```

Watching and debugging application assets
```sh
$ gulp watch
```

Just Compiling the assets (html,css,js,images)
```sh
$ gulp compile
```

### Internal task commands

Internal task commands used by watching and serve

Application JS compiling:
```sh
$ gulp build-js
```

Vendor JS compiling:
```sh
$ gulp build-vendor-js
```

Vendor JS wrapping and building:
```sh
$ gulp pug
```

Application CSS compiling:
```sh
$ gulp build-css
```

Vendor CSS compiling::
```sh
$ gulp build-vendor-css
```

Application image files compiling:
```sh
$ gulp image
```
