    //init
    var app = angular.module('appFlix', ['ngMaterial', 'ngMessages']);
    
    //controller da aplicação
    app.controller('appFlixController', function($scope, $http) {
        
        //separa as 2 urls de api que iremos trabalhar em variáveis específicas
        var showInfo = 'https://api.jsonbin.io/b/5c4901ea04ce8017ee29b895';
        var showEpis = 'https://api.jsonbin.io/b/5c4901bf6dbfe317d4c37828';
        
        var uniqueNames = [];  
        //puxa os dados da url da variável showEpis
        $http.get(showEpis).then(function (response) {
            $scope.Episodes = response.data;
            //cria um array novo de escopo para separa as categorias usadas pela lista
            for(i = 0; i< $scope.Episodes.length-1; i++){ 
                if ($scope.Episodes[i]!==null){
                    if(uniqueNames.indexOf($scope.Episodes[i].SeasonNumber) === -1){
                        uniqueNames.push($scope.Episodes[i].SeasonNumber);        
                    }  
                }

            }
            $scope.Seasons = uniqueNames;
        });
        
            //puxa os dados da url da variável showInfo
        $http.get(showInfo).then(function (response) {
            $scope.Info = response.data;
            $scope.Team = response.data.Team;
            $scope.Genres = response.data.Genres;
        });                   
        
    });
app.directive('backImg', function(){
    return function(scope, element, attrs){
        var url = scope.$eval(attrs.backImg);
        attrs.$observe('backImg', function(value) {
            element.css({
                'background-image': 'url(' + value +')'
            });
        });
    };
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb250cm9sZXIuanMiLCJhcHAuZGlyZWN0aXZlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDakNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiAgICAvL2luaXRcclxuICAgIHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnYXBwRmxpeCcsIFsnbmdNYXRlcmlhbCcsICduZ01lc3NhZ2VzJ10pO1xyXG4gICAgXHJcbiAgICAvL2NvbnRyb2xsZXIgZGEgYXBsaWNhw6fDo29cclxuICAgIGFwcC5jb250cm9sbGVyKCdhcHBGbGl4Q29udHJvbGxlcicsIGZ1bmN0aW9uKCRzY29wZSwgJGh0dHApIHtcclxuICAgICAgICBcclxuICAgICAgICAvL3NlcGFyYSBhcyAyIHVybHMgZGUgYXBpIHF1ZSBpcmVtb3MgdHJhYmFsaGFyIGVtIHZhcmnDoXZlaXMgZXNwZWPDrWZpY2FzXHJcbiAgICAgICAgdmFyIHNob3dJbmZvID0gJ2h0dHBzOi8vYXBpLmpzb25iaW4uaW8vYi81YzQ5MDFlYTA0Y2U4MDE3ZWUyOWI4OTUnO1xyXG4gICAgICAgIHZhciBzaG93RXBpcyA9ICdodHRwczovL2FwaS5qc29uYmluLmlvL2IvNWM0OTAxYmY2ZGJmZTMxN2Q0YzM3ODI4JztcclxuICAgICAgICBcclxuICAgICAgICB2YXIgdW5pcXVlTmFtZXMgPSBbXTsgIFxyXG4gICAgICAgIC8vcHV4YSBvcyBkYWRvcyBkYSB1cmwgZGEgdmFyacOhdmVsIHNob3dFcGlzXHJcbiAgICAgICAgJGh0dHAuZ2V0KHNob3dFcGlzKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAkc2NvcGUuRXBpc29kZXMgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgICAgICAvL2NyaWEgdW0gYXJyYXkgbm92byBkZSBlc2NvcG8gcGFyYSBzZXBhcmEgYXMgY2F0ZWdvcmlhcyB1c2FkYXMgcGVsYSBsaXN0YVxyXG4gICAgICAgICAgICBmb3IoaSA9IDA7IGk8ICRzY29wZS5FcGlzb2Rlcy5sZW5ndGgtMTsgaSsrKXsgXHJcbiAgICAgICAgICAgICAgICBpZiAoJHNjb3BlLkVwaXNvZGVzW2ldIT09bnVsbCl7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYodW5pcXVlTmFtZXMuaW5kZXhPZigkc2NvcGUuRXBpc29kZXNbaV0uU2Vhc29uTnVtYmVyKSA9PT0gLTEpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1bmlxdWVOYW1lcy5wdXNoKCRzY29wZS5FcGlzb2Rlc1tpXS5TZWFzb25OdW1iZXIpOyAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgfSAgXHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICRzY29wZS5TZWFzb25zID0gdW5pcXVlTmFtZXM7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgICAgIC8vcHV4YSBvcyBkYWRvcyBkYSB1cmwgZGEgdmFyacOhdmVsIHNob3dJbmZvXHJcbiAgICAgICAgJGh0dHAuZ2V0KHNob3dJbmZvKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAkc2NvcGUuSW5mbyA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICAgICRzY29wZS5UZWFtID0gcmVzcG9uc2UuZGF0YS5UZWFtO1xyXG4gICAgICAgICAgICAkc2NvcGUuR2VucmVzID0gcmVzcG9uc2UuZGF0YS5HZW5yZXM7XHJcbiAgICAgICAgfSk7ICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIFxyXG4gICAgfSk7IiwiYXBwLmRpcmVjdGl2ZSgnYmFja0ltZycsIGZ1bmN0aW9uKCl7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24oc2NvcGUsIGVsZW1lbnQsIGF0dHJzKXtcclxuICAgICAgICB2YXIgdXJsID0gc2NvcGUuJGV2YWwoYXR0cnMuYmFja0ltZyk7XHJcbiAgICAgICAgYXR0cnMuJG9ic2VydmUoJ2JhY2tJbWcnLCBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgICAgICAgICBlbGVtZW50LmNzcyh7XHJcbiAgICAgICAgICAgICAgICAnYmFja2dyb3VuZC1pbWFnZSc6ICd1cmwoJyArIHZhbHVlICsnKSdcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59KTsiXX0=
