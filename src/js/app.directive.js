app.directive('backImg', function(){
    return function(scope, element, attrs){
        var url = scope.$eval(attrs.backImg);
        attrs.$observe('backImg', function(value) {
            element.css({
                'background-image': 'url(' + value +')'
            });
        });
    };
});