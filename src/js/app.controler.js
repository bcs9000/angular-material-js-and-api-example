    //init
    var app = angular.module('appFlix', ['ngMaterial', 'ngMessages']);
    
    //controller da aplicação
    app.controller('appFlixController', function($scope, $http) {
        
        //separa as 2 urls de api que iremos trabalhar em variáveis específicas
        var showInfo = 'https://api.jsonbin.io/b/5c4901ea04ce8017ee29b895';
        var showEpis = 'https://api.jsonbin.io/b/5c4901bf6dbfe317d4c37828';
        
        var uniqueNames = [];  
        //puxa os dados da url da variável showEpis
        $http.get(showEpis).then(function (response) {
            $scope.Episodes = response.data;
            //cria um array novo de escopo para separa as categorias usadas pela lista
            for(i = 0; i< $scope.Episodes.length-1; i++){ 
                if ($scope.Episodes[i]!==null){
                    if(uniqueNames.indexOf($scope.Episodes[i].SeasonNumber) === -1){
                        uniqueNames.push($scope.Episodes[i].SeasonNumber);        
                    }  
                }

            }
            $scope.Seasons = uniqueNames;
        });
        
            //puxa os dados da url da variável showInfo
        $http.get(showInfo).then(function (response) {
            $scope.Info = response.data;
            $scope.Team = response.data.Team;
            $scope.Genres = response.data.Genres;
        });                   
        
    });